-----------------------------------------------------------------------
--FourBitAdder
--Michael Sargent, Designer Extraoridnaire
--4/23/17
--Revision 1.0
-----------------------------------------------------------------------

--Library declaration
library IEEE;
use IEEE.std_logic_1164.ALL;

--Entity declaration
entity FourBitAdder is
port ( A : in bit_vector(3 downto 0);
		 B : in bit_vector(3 downto 0);
		 cin : in bit;
		 led : out bit_vector(3 downto 0);
		 led4 : out bit
		);
end FourBitAdder;


--Arcitecture Body
architecture Behavioral of FourBitAdder is

Component FullAdder1 is
port (x : in bit;
		y : in bit;
		cin : in bit;
		sum : out bit;
		carry : out bit
);
end component;

signal s : bit_vector (2 downto 0);

begin

sum_0 : FullAdder1 port map (a(0),b(0),'0',led(0),s(0));
sum_1 : FullAdder1 port map (a(1),b(1),s(0),led(1),s(1));
sum_2 : FullAdder1 port map (a(2),b(2),s(1),led(2),s(2));
sum_3 : FullAdder1 port map (a(3),b(3),s(2),led(3),led4);

end Behavioral;
