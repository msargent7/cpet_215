-- Copyright (C) 2016  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Intel and sold by Intel or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 16.1.0 Build 196 10/24/2016 SJ Lite Edition"

-- DATE "04/17/2017 23:00:23"

-- 
-- Device: Altera 5CEBA4F23C7 Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA_LNSIM;
LIBRARY CYCLONEV;
LIBRARY IEEE;
USE ALTERA_LNSIM.ALTERA_LNSIM_COMPONENTS.ALL;
USE CYCLONEV.CYCLONEV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	Demux_Lab_VHDL IS
    PORT (
	sw1 : IN std_logic;
	sw0 : IN std_logic;
	D : IN std_logic;
	led3 : BUFFER std_logic;
	led2 : BUFFER std_logic;
	led1 : BUFFER std_logic;
	led0 : BUFFER std_logic
	);
END Demux_Lab_VHDL;

ARCHITECTURE structure OF Demux_Lab_VHDL IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_sw1 : std_logic;
SIGNAL ww_sw0 : std_logic;
SIGNAL ww_D : std_logic;
SIGNAL ww_led3 : std_logic;
SIGNAL ww_led2 : std_logic;
SIGNAL ww_led1 : std_logic;
SIGNAL ww_led0 : std_logic;
SIGNAL \led3~output_o\ : std_logic;
SIGNAL \led2~output_o\ : std_logic;
SIGNAL \led1~output_o\ : std_logic;
SIGNAL \led0~output_o\ : std_logic;
SIGNAL \sw0~input_o\ : std_logic;
SIGNAL \sw1~input_o\ : std_logic;
SIGNAL \D~input_o\ : std_logic;
SIGNAL \led1~0_combout\ : std_logic;
SIGNAL \led1~1_combout\ : std_logic;
SIGNAL \led1~2_combout\ : std_logic;
SIGNAL \led0~0_combout\ : std_logic;
SIGNAL \ALT_INV_D~input_o\ : std_logic;
SIGNAL \ALT_INV_sw1~input_o\ : std_logic;
SIGNAL \ALT_INV_sw0~input_o\ : std_logic;

BEGIN

ww_sw1 <= sw1;
ww_sw0 <= sw0;
ww_D <= D;
led3 <= ww_led3;
led2 <= ww_led2;
led1 <= ww_led1;
led0 <= ww_led0;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\ALT_INV_D~input_o\ <= NOT \D~input_o\;
\ALT_INV_sw1~input_o\ <= NOT \sw1~input_o\;
\ALT_INV_sw0~input_o\ <= NOT \sw0~input_o\;

\led3~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \led1~0_combout\,
	devoe => ww_devoe,
	o => \led3~output_o\);

\led2~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \led1~1_combout\,
	devoe => ww_devoe,
	o => \led2~output_o\);

\led1~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \led1~2_combout\,
	devoe => ww_devoe,
	o => \led1~output_o\);

\led0~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \led0~0_combout\,
	devoe => ww_devoe,
	o => \led0~output_o\);

\sw0~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_sw0,
	o => \sw0~input_o\);

\sw1~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_sw1,
	o => \sw1~input_o\);

\D~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D,
	o => \D~input_o\);

\led1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \led1~0_combout\ = (\sw0~input_o\ & (\sw1~input_o\ & \D~input_o\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000100000001000000010000000100000001000000010000000100000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_sw0~input_o\,
	datab => \ALT_INV_sw1~input_o\,
	datac => \ALT_INV_D~input_o\,
	combout => \led1~0_combout\);

\led1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \led1~1_combout\ = (!\sw0~input_o\ & (\sw1~input_o\ & \D~input_o\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001000000010000000100000001000000010000000100000001000000010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_sw0~input_o\,
	datab => \ALT_INV_sw1~input_o\,
	datac => \ALT_INV_D~input_o\,
	combout => \led1~1_combout\);

\led1~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \led1~2_combout\ = (\sw0~input_o\ & (!\sw1~input_o\ & \D~input_o\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010000000100000001000000010000000100000001000000010000000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_sw0~input_o\,
	datab => \ALT_INV_sw1~input_o\,
	datac => \ALT_INV_D~input_o\,
	combout => \led1~2_combout\);

\led0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \led0~0_combout\ = (!\sw0~input_o\ & (!\sw1~input_o\ & \D~input_o\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000100000001000000010000000100000001000000010000000100000001000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_sw0~input_o\,
	datab => \ALT_INV_sw1~input_o\,
	datac => \ALT_INV_D~input_o\,
	combout => \led0~0_combout\);

ww_led3 <= \led3~output_o\;

ww_led2 <= \led2~output_o\;

ww_led1 <= \led1~output_o\;

ww_led0 <= \led0~output_o\;
END structure;


