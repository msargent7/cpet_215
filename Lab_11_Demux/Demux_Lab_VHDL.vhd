-----------------------------------------------------------------------
--Demux_Lab_VHDL
--Michael Sargent, Designer Extraoridnaire
--4/17/17
--Revision 1.0
-----------------------------------------------------------------------

--Library declaration
library IEEE;
use IEEE.std_logic_1164.ALL;

--Entity declaration
entity Demux_Lab_VHDL is
Port ( sw1	: in bit;
		 sw0	: in bit;
		 D		: in bit;
		 led3	: out bit;
		 led2	: out bit;
		 led1	: out bit;
		 led0	: out bit);
end Demux_Lab_VHDL;

--Arcitecture Body
architecture Behavioral of Demux_Lab_VHDL is
begin

	led3 <= sw1 and sw0 and D;
	led2 <= sw1 and not sw0 and D;
	led1 <= not sw1 and sw0 and D;
	led0 <= not sw1 and not sw0 and D;
	
	end Behavioral;