---------------------------------------------------------------
--not_gate.vhd, 
--Michael Sargnet, Designer
--Date
--Revision 1.0
---------------------------------------------------------------

--Library declaration
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

--Entity and architecture for not_gate
entity not_gate is
	PORT ( a : in  bit;
			 x : out bit);
end not_gate;

--Architecture body
architecture not_function of not_gate is
begin

	x <= not a;

	end not_function;