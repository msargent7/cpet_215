-- Copyright (C) 2016  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Intel and sold by Intel or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- *****************************************************************************
-- This file contains a Vhdl test bench with test vectors .The test vectors     
-- are exported from a vector file in the Quartus Waveform Editor and apply to  
-- the top level entity of the current Quartus project .The user can use this   
-- testbench to simulate his design using a third-party simulation tool .       
-- *****************************************************************************
-- Generated on "04/17/2017 22:24:18"
                                                             
-- Vhdl Test Bench(with test vectors) for design  :          LED_Structural_VHDL
-- 
-- Simulation tool : 3rd Party
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY LED_Structural_VHDL_vhd_vec_tst IS
END LED_Structural_VHDL_vhd_vec_tst;
ARCHITECTURE LED_Structural_VHDL_arch OF LED_Structural_VHDL_vhd_vec_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL led0 : STD_LOGIC;
SIGNAL led1 : STD_LOGIC;
SIGNAL led2 : STD_LOGIC;
SIGNAL led3 : STD_LOGIC;
SIGNAL sw0 : STD_LOGIC;
SIGNAL sw1 : STD_LOGIC;
COMPONENT LED_Structural_VHDL
	PORT (
	led0 : BUFFER STD_LOGIC;
	led1 : BUFFER STD_LOGIC;
	led2 : BUFFER STD_LOGIC;
	led3 : BUFFER STD_LOGIC;
	sw0 : IN STD_LOGIC;
	sw1 : IN STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : LED_Structural_VHDL
	PORT MAP (
-- list connections between master ports and signals
	led0 => led0,
	led1 => led1,
	led2 => led2,
	led3 => led3,
	sw0 => sw0,
	sw1 => sw1
	);

-- sw1
t_prcs_sw1: PROCESS
BEGIN
	sw1 <= '0';
	WAIT FOR 100000 ps;
	sw1 <= '1';
WAIT;
END PROCESS t_prcs_sw1;

-- sw0
t_prcs_sw0: PROCESS
BEGIN
	sw0 <= '0';
	WAIT FOR 50000 ps;
	sw0 <= '1';
	WAIT FOR 50000 ps;
	sw0 <= '0';
	WAIT FOR 50000 ps;
	sw0 <= '1';
WAIT;
END PROCESS t_prcs_sw0;
END LED_Structural_VHDL_arch;
