onerror {exit -code 1}
vlib work
vcom -work work LED_Structural_VHDL.vho
vcom -work work LED_Struct.vwf.vht
vsim -novopt -c -t 1ps -L cyclonev -L altera -L altera_mf -L 220model -L sgate -L altera_lnsim work.LED_Structural_VHDL_vhd_vec_tst
vcd file -direction LED_Structural_VHDL.msim.vcd
vcd add -internal LED_Structural_VHDL_vhd_vec_tst/*
vcd add -internal LED_Structural_VHDL_vhd_vec_tst/i1/*
proc simTimestamp {} {
    echo "Simulation time: $::now ps"
    if { [string equal running [runStatus]] } {
        after 2500 simTimestamp
    }
}
after 2500 simTimestamp
run -all
quit -f
