---------------------------------------------------------------
--and_gate2.vhd, 
--Michael Sargnet, Designer
--Date
--Revision 1.0
---------------------------------------------------------------

--Library declaration
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

--Entity and architecture for and_gate2
entity and_gate2 is
	PORT ( a : in  bit;
			 b : in  bit;
			 x : out bit);
end and_gate2;

--Architecture body
architecture and_function of and_gate2 is
begin

	x <= a and b;

	end and_function;