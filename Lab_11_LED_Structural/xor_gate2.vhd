---------------------------------------------------------------
--xor_gate2.vhd, 
--Michael Sargnet, Designer
--Date
--Revision 1.0
---------------------------------------------------------------

--Library declaration
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

--Entity and architecture for xor_gate2
entity xor_gate2 is
	PORT ( a : in  bit;
			 b : in  bit;
			 x : out bit);
end xor_gate2;

--Architecture body
architecture xor_function of xor_gate2 is
begin

	x <= a xor b;

	end xor_function;