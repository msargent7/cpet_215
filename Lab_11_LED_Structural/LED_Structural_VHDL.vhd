---------------------------------------------------------------
--LED_Structura_VHDL, 
--Michael Sargnet, Designer
--Date
--Revision 1.0
---------------------------------------------------------------

--Library declaration
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

--Entity and architecture for LED_VHDL_Structural
entity LED_Structural_VHDL is
	Port ( sw1	: in bit;
			 sw0  : in bit;
			 led3 : out bit;
			 led2 : out bit;
			 led1 : out bit;
			 led0 : out bit);
end LED_Structural_VHDL;

architecture Behavioral of LED_Structural_VHDL is

--compenent declaration for and2_gate
component and_gate2 is
	port (a, b : in bit; x : out bit);
end component and_gate2;

--component declaration for xor2_gate
component xor_gate2 is
	port (a, b : in bit; x : out bit);
end component xor_gate2;

--component declaration for not_gate
component not_gate is
	port (a : in bit; x : out bit);
end component not_gate;


signal out1, out0 : bit;

begin

	led3 <= sw1;
	led2 <= sw0;
	led1 <= not (sw1 and sw0);
	led0 <= not (sw1 xor sw0);
	
end Behavioral;