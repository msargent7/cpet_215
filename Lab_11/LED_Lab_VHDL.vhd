-----------------------------------------------------------------------
--LED_Lab_VHDL
--Michael Sargent, Designer Extraoridnaire
--4/17/17
--Revision 1.0
-----------------------------------------------------------------------

--Library declaration
library IEEE;
use IEEE.std_logic_1164.ALL;

--Entity declaration
entity LED_Lab_VHDL is
Port ( sw1	: in bit;
		 sw0	: in bit;
		 led3	: out bit;
		 led2	: out bit;
		 led1	: out bit;
		 led0	: out bit);
end LED_Lab_VHDL;

--Arcitecture Body
architecture Behavioral of LED_Lab_VHDL is
begin

	led3 <= sw1;
	led2 <= sw0;
	led1 <= sw1 and sw0;
	led0 <= sw1 xor sw0;
	
	end Behavioral;