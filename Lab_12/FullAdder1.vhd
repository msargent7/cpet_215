-----------------------------------------------------------------------
--FullAdder1
--Michael Sargent, Designer Extraoridnaire
--4/23/17
--Revision 1.0
-----------------------------------------------------------------------

--Library declaration
library IEEE;
use IEEE.std_logic_1164.ALL;

--Entity declaration
entity FullAdder1 is
port (x : in bit;
		y : in bit;
		cin : in bit;
		sum : out bit;
		carry : out bit
);
end FullAdder1;

--Arcitecture Body
architecture Behavioral of FullAdder1 is
begin

sum <= x XOR y XOR cin;
carry <= (x AND y) OR (x AND cin) or (y AND cin);

end Behavioral;
