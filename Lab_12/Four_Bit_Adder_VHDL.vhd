-----------------------------------------------------------------------
--Four_Bit_Adder_VHDL
--Michael Sargent, Designer Extraoridnaire
--4/23/17
--Revision 1.0
-----------------------------------------------------------------------

--Library declaration
library IEEE;
use IEEE.std_logic_1164.ALL;

--Entity declaration
entity Four_Bit_Adder_VHDL is
port ( A : in bit_vector(3 downto 0);
		 B : in bit_vector(7 downto 4);
		 CIN : in bit;
		 led : out bit_vector(3 downto 0);
		 led4 : out bit
		);
end Four_Bit_Adder_VHDL;

--Arcitecture Body
architecture Behavioral of Four_Bit_Adder_VHDL is
begin


end Behavioral;
